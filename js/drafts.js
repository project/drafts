(function ($) {

  // @todo we need a delegation system here so that other modules can react to events (e.g. wysiwyg)

  Drupal.drafts = {
    timer : null,
    frequency : null,
    run : function() {
      // We'll isolate our setTimeout here so that we can
      // dynamically adjust the frequency.
      Drupal.drafts.timer = setTimeout(Drupal.drafts.save, Drupal.drafts.frequency);
    },
    save : function() {
      Drupal.drafts.send();
    },
    send : function() {
      // Scrape the form data.
      var form = $('#save-draft').parents('form:first').serialize() + "&op=" + $('#save-draft').val();
  
      // We're using $.ajax() here so that we can have more control 
      // over the process. 
      $.ajax({
        type: 'POST',
        url: '/system/ajax',
        data: form,
        success: function (data, textStatus, jqXHR) {

          // Since we're just scraping the form data, we only care
          // about the drafts status message and can ignore the rest.
          for (var i in data) {
            if (data[i]['command'] == 'insert' && typeof(data[i]['command']) !== 'undefined' && data[i]['data'].match(/drafts-last-saved/)) {
              $('#drafts-status').html(data[i]['data']);
            } 
          }

          // Initiate the next interval.
          Drupal.drafts.run();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          clearTimeout(Drupal.drafts.timer);
          console.log(errorThrown);
        },
        dataType: 'json'
      }); 
    }
  };

  $(document).ready(function() {
    $('#save-draft').click(function(e) {
      e.preventDefault();
      Drupal.drafts.send();
    });

    if (typeof(Drupal.settings.drafts.autosave) && Drupal.settings.drafts.autosave) {
      Drupal.drafts.frequency = Drupal.settings.drafts.frequency * 1000;
      var t = setTimeout()
      Drupal.drafts.timer = setTimeout(Drupal.drafts.run, Drupal.drafts.frequency);
    }
  });

})(jQuery);
